object fSettings: TfSettings
  Left = 1002
  Top = 360
  BorderStyle = bsToolWindow
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 361
  ClientWidth = 305
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object Label1: TLabel
    Left = 8
    Top = 168
    Width = 230
    Height = 16
    Caption = #1043#1086#1088#1103#1095#1072#1103' '#1082#1083#1072#1074#1080#1096#1072' '#1076#1083#1103' '#1086#1095#1080#1089#1090#1082#1080' '#1083#1086#1075#1072':'
  end
  object cbShowDots: TCheckBox
    Left = 8
    Top = 8
    Width = 287
    Height = 17
    Cursor = crHandPoint
    Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1091#1088#1086#1085' '#1086#1090' '#1076#1086#1090#1086#1074' '#1074' '#1090#1072#1073#1083#1080#1094#1077
    TabOrder = 0
    OnClick = cbShowDotsClick
  end
  object cbHideMobs: TCheckBox
    Left = 8
    Top = 32
    Width = 287
    Height = 17
    Cursor = crHandPoint
    Caption = #1057#1082#1088#1099#1090#1100' '#1084#1086#1073#1086#1074' '#1074' '#1090#1072#1073#1083#1080#1094#1077
    TabOrder = 1
    OnClick = cbHideMobsClick
  end
  object bSkillList: TButton
    Left = 8
    Top = 277
    Width = 129
    Height = 25
    Cursor = crHandPoint
    Caption = #1041#1072#1079#1072' '#1089#1082#1080#1083#1083#1086#1074
    TabOrder = 6
    OnClick = bSkillListClick
  end
  object bSkillDotList: TButton
    Left = 166
    Top = 277
    Width = 129
    Height = 25
    Cursor = crHandPoint
    Caption = #1041#1072#1079#1072' '#1044#1054#1058#1086#1074
    TabOrder = 7
    OnClick = bSkillDotListClick
  end
  object RadioGroup1: TRadioGroup
    Left = 8
    Top = 55
    Width = 287
    Height = 105
    Caption = #1042#1099#1073#1086#1088' '#1084#1077#1090#1086#1076#1072' '#1088#1072#1089#1095#1105#1090#1072' '#1044#1055#1057' '#1089' '#1076#1086#1090#1086#1074
    TabOrder = 8
  end
  object rbDotsToLast: TRadioButton
    Left = 24
    Top = 81
    Width = 257
    Height = 32
    Cursor = crHandPoint
    Hint = 
      #1041#1086#1083#1077#1077' '#1087#1088#1072#1074#1080#1083#1100#1085#1099#1081' '#1084#1077#1090#1086#1076' '#1077#1089#1083#1080' '#1074#1099' '#1080#1075#1088#1072#1077#1090#1077' '#1079#1072' '#1082#1083#1072#1089#1089' '#1095#1090#1086' '#1085#1077' '#1080#1084#1077#1077#1090' '#1076#1086#1090 +
      #1086#1074', '#1073#1086#1083#1100#1096#1077' '#1087#1086#1076#1093#1086#1076#1080#1090' '#1076#1083#1103' '#1043#1083#1072#1076#1080#1072#1090#1086#1088#1072','#1058#1072#1085#1082#1072', '#1055#1080#1083#1086#1090#1072', '#1063#1072#1088#1086#1076#1077#1103', '#1051#1091#1082#1072',' +
      ' '#1057#1080#1085#1072
    Caption = #1053#1072#1095#1080#1089#1083#1103#1090#1100' '#1044#1054#1058#1099' '#1087#1086#1089#1083#1077#1076#1085#1077#1084#1091', '#1082#1090#1086' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1083' '#1091#1084#1077#1085#1080#1077'-'#1044#1054#1058
    Checked = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    TabStop = True
    WordWrap = True
    OnClick = rbDotsDividingClick
  end
  object rbDotsDividing: TRadioButton
    Left = 24
    Top = 119
    Width = 257
    Height = 33
    Cursor = crHandPoint
    Hint = #1042#1089#1077' '#1076#1086#1090#1099' '#1073#1091#1076#1091#1090' '#1088#1072#1079#1076#1077#1083#1077#1085#1099' '#1084#1077#1078#1076#1091' '#1086#1076#1080#1085#1072#1082#1086#1074#1099#1084#1080' '#1082#1083#1072#1089#1089#1072#1084#1080
    Caption = #1044#1077#1083#1080#1090#1100' '#1087#1086#1088#1086#1074#1085#1091' '#1091#1088#1086#1085' '#1089' '#1044#1054#1058#1086#1074' '#1084#1077#1078#1076#1091' '#1086#1076#1080#1085#1072#1082#1086#1074#1099#1084#1080' '#1082#1083#1072#1089#1089#1072#1084#1080
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    WordWrap = True
    OnClick = rbDotsDividingClick
  end
  object HotKey: THotKey
    Left = 8
    Top = 190
    Width = 287
    Height = 22
    HotKey = 122
    InvalidKeys = []
    Modifiers = []
    TabOrder = 4
    OnChange = HotKeyChange
  end
  object lbledtYoursCharName: TLabeledEdit
    Left = 8
    Top = 239
    Width = 287
    Height = 24
    EditLabel.Width = 154
    EditLabel.Height = 16
    EditLabel.Caption = #1048#1084#1103' '#1074#1072#1096#1077#1075#1086' '#1087#1077#1088#1089#1086#1085#1072#1078#1072':'
    TabOrder = 5
    Text = '!'#1042#1067'!'
    OnChange = lbledtYoursCharNameChange
  end
  object btnOK: TButton
    Left = 40
    Top = 320
    Width = 225
    Height = 25
    Cursor = crHandPoint
    Caption = 'OK'
    Default = True
    TabOrder = 9
    OnClick = btnOKClick
  end
end

unit uAdvanced;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, GridsEh, DBAxisGridsEh, DBGridEh, Data.DB,
  EhLibVCL;

type
  TfrmAdvanced = class(TForm)
    DBGridEh1: TDBGridEh;
    DS: TDataSource;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdvanced: TfrmAdvanced;

implementation

{$R *.dfm}

uses uMain;

procedure TfrmAdvanced.FormCreate(Sender: TObject);
begin
  frmAdvanced.Top  := frmMain.Top;
  frmAdvanced.Left := frmMain.Left-frmAdvanced.Width;
end;

end.

object frmStat: TfrmStat
  Left = 1202
  Top = 270
  Caption = 'frmStat'
  ClientHeight = 361
  ClientWidth = 534
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 120
  TextHeight = 16
  object DBGridEh: TDBGridEh
    Left = 0
    Top = 0
    Width = 534
    Height = 361
    Align = alClient
    AutoFitColWidths = True
    DataSource = DS
    DynProps = <>
    FooterParams.Color = clWindow
    OddRowColor = clInfoBk
    ReadOnly = True
    TabOrder = 0
    Columns = <
      item
        AutoFitColWidth = False
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'ClassNo'
        Footers = <>
        ImageList = frmMain.ImgList
        Title.Caption = #1050#1083#1072#1089#1089
        Width = 45
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'SkillName'
        Footers = <>
        Title.Caption = #1057#1082#1080#1083#1083
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'LastUsed'
        Footers = <>
        Title.Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1083
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DS: TDataSource
    Left = 232
    Top = 144
  end
end

unit uSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Vcl.ComCtrls;

type
  TfSettings = class(TForm)
    cbShowDots: TCheckBox;
    cbHideMobs: TCheckBox;
    bSkillList: TButton;
    bSkillDotList: TButton;
    RadioGroup1: TRadioGroup;
    rbDotsToLast: TRadioButton;
    rbDotsDividing: TRadioButton;
    HotKey: THotKey;
    Label1: TLabel;
    lbledtYoursCharName: TLabeledEdit;
    btnOK: TButton;
    procedure cbShowDotsClick(Sender: TObject);
    procedure cbHideMobsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bSkillListClick(Sender: TObject);
    procedure bSkillDotListClick(Sender: TObject);
    procedure rbDotsDividingClick(Sender: TObject);
    procedure HotKeyChange(Sender: TObject);
    procedure lbledtYoursCharNameChange(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

{
const
   SECURITY_NT_AUTHORITY: TSIDIdentifierAuthority =
     (Value: (0, 0, 0, 0, 0, 5));
   SECURITY_BUILTIN_DOMAIN_RID = $00000020;
   DOMAIN_ALIAS_RID_ADMINS = $00000220;
}

var
  fSettings: TfSettings;

implementation

uses uMain, uStat;

{$R *.dfm}
(*
function IsRunAsAdmin: Boolean;
 var
   hAccessToken: THandle;
   ptgGroups: PTokenGroups;
   dwInfoBufferSize: DWORD;
   psidAdministrators: PSID;
   x: Integer;
   bSuccess: BOOL;
 begin
   Result   := False;
   bSuccess := OpenThreadToken(GetCurrentThread, TOKEN_QUERY, True,
     hAccessToken);
   if not bSuccess then
   begin
     if GetLastError = ERROR_NO_TOKEN then
       bSuccess := OpenProcessToken(GetCurrentProcess, TOKEN_QUERY,
         hAccessToken);
   end;
   if bSuccess then
   begin
     GetMem(ptgGroups, 1024);
     bSuccess := GetTokenInformation(hAccessToken, TokenGroups,
       ptgGroups, 1024, dwInfoBufferSize);
     CloseHandle(hAccessToken);
     if bSuccess then
     begin
       AllocateAndInitializeSid(SECURITY_NT_AUTHORITY, 2,
         SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS,
         0, 0, 0, 0, 0, 0, psidAdministrators);
       {$R-}
       for x := 0 to ptgGroups.GroupCount - 1 do
         if EqualSid(psidAdministrators, ptgGroups.Groups[x].Sid) then
         begin
           Result := True;
           Break;
         end;
       {$R+}
       FreeSid(psidAdministrators);
     end;
     FreeMem(ptgGroups);
   end;
 end;
*)

procedure TfSettings.FormCreate(Sender: TObject);
begin
  fSettings.cbShowDots.Checked := optShowDots = 1;
  fSettings.cbHideMobs.Checked := optHideMobs = 1;

  if optCalcType = 0 then
    rbDotsToLast.Checked := True
  else
    rbDotsDividing.Checked := True;

  HotKey.HotKey := cHotKey;
  if cHotKeyModifiers and MOD_CONTROL <> 0 then
     HotKey.Modifiers := HotKey.Modifiers+[hkCtrl];
  if cHotKeyModifiers and MOD_SHIFT <> 0 then
     HotKey.Modifiers := HotKey.Modifiers+[hkShift];
  if cHotKeyModifiers and MOD_ALT <> 0 then
     HotKey.Modifiers := HotKey.Modifiers+[hkAlt];
  //
  lbledtYoursCharName.Text := sYoursCharName;
end;

procedure TfSettings.HotKeyChange(Sender: TObject);
begin
  frmMain.UnregHC;
  //
  if HotKey.HotKey=VK_F12 then//The F12 key is reserved for use by the debugger at all times, so it should not be registered as a hot key
     HotKey.HotKey := 0;

  cHotKey := LoWord(LoByte(HotKey.HotKey));
  cHotKeyModifiers := 0;
  if (cHotKey<>0) then
  begin
    if (hkCtrl in HotKey.Modifiers) then
       cHotKeyModifiers := cHotKeyModifiers or MOD_CONTROL;
    if (hkShift in HotKey.Modifiers) then
       cHotKeyModifiers := cHotKeyModifiers or MOD_SHIFT;
    if (hkAlt in HotKey.Modifiers) then
       cHotKeyModifiers := cHotKeyModifiers or MOD_ALT;

    frmMain.RegHC;

    //if not IsRunAsAdmin then
    //   ShowMessage('��� ���� ����� ������� ������� �������� � Aion (�����) ��������� ��������� �� ����� ��������������');
  end;
end;

procedure TfSettings.lbledtYoursCharNameChange(Sender: TObject);
begin
  if lbledtYoursCharName.Text<>'' then
    sYoursCharName := lbledtYoursCharName.Text
  else
    sYoursCharName := '!��!';
end;

procedure TfSettings.rbDotsDividingClick(Sender: TObject);
begin
  if rbDotsDividing.Checked then
  begin
    optCalcType := 1;
  end
  else
    optCalcType := 0;
end;

procedure TfSettings.cbShowDotsClick(Sender: TObject);
begin
  if cbShowDots.Checked then
  begin
    optShowDots := 1;
    frmMain.DBGrid.Columns.Items[3].Visible := True;
    frmMain.DBGrid.Columns.Items[4].Visible := True;
  end
  else
  begin
    optShowDots := 0;
    frmMain.DBGrid.Columns.Items[3].Visible := False;
    frmMain.DBGrid.Columns.Items[4].Visible := False;
  end;
end;

procedure TfSettings.cbHideMobsClick(Sender: TObject);
begin
  if cbHideMobs.Checked then
  begin
    optHideMobs := 1;
    frmMain.mtList.Filter := 'CharClass<>0';
    frmMain.mtList.Filtered := True;
  end
  else
  begin
    optHideMobs := 0;
    frmMain.mtList.Filter := '';
    frmMain.mtList.Filtered := False;
  end;
end;

procedure TfSettings.bSkillListClick(Sender: TObject);
begin
  frmStat.Caption := '������ ����������';
  frmStat.Left := frmMain.Left + 5;
  frmStat.Top := frmMain.Top + 130;
  frmStat.Width := 550;
  frmStat.Height := 400;
  frmStat.DS.DataSet := frmMain.mtSkills;
  frmStat.Show;
end;

procedure TfSettings.btnOKClick(Sender: TObject);
begin
  fSettings.Close;
end;

procedure TfSettings.bSkillDotListClick(Sender: TObject);
begin
  frmStat.Caption := '���� � ��������� ��� �����������';
  frmStat.Left := frmMain.Left + 5;
  frmStat.Top := frmMain.Top + 130;
  frmStat.Width := 550;
  frmStat.Height := 400;
  frmStat.DS.DataSet := frmMain.mtDots;
  frmStat.Show;
end;

end.

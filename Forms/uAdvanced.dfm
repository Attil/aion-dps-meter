object frmAdvanced: TfrmAdvanced
  Left = 0
  Top = 0
  Caption = #1056#1072#1089#1096#1080#1088#1077#1085#1085#1072#1103' '#1089#1090#1072#1090#1080#1089#1090#1080#1082#1072' ('#1087#1086' '#1089#1082#1080#1083#1083#1072#1084')'
  ClientHeight = 513
  ClientWidth = 771
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 0
    Width = 771
    Height = 513
    Align = alClient
    AutoFitColWidths = True
    DataSource = DS
    DynProps = <>
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    TabOrder = 0
    Columns = <
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Char'
        Footers = <>
        Title.Alignment = taCenter
        Title.Caption = #1055#1077#1088#1089#1086#1085#1072#1078
        Width = 149
      end
      item
        AutoFitColWidth = False
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Class'
        Footers = <>
        ImageList = frmMain.ImgList
        Title.Alignment = taCenter
        Title.Caption = #1050#1083'.'
        Width = 37
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'Skill'
        Footers = <>
        Title.Alignment = taCenter
        Title.Caption = #1057#1082#1080#1083#1083
        Width = 213
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'AttCnt'
        Footers = <>
        Title.Alignment = taCenter
        Title.Caption = #1040#1090#1072#1082
        Width = 54
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'CritCnt'
        Footers = <>
        Title.Alignment = taCenter
        Title.Caption = #1050#1088#1080#1090#1086#1074
        Width = 46
      end
      item
        CellButtons = <>
        DynProps = <>
        EditButtons = <>
        FieldName = 'AttDmg'
        Footers = <>
        Title.Alignment = taCenter
        Title.Caption = #1059#1088#1086#1085
        Width = 75
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DS: TDataSource
    DataSet = frmMain.mtAdv
    Left = 472
    Top = 48
  end
end

unit uAbout;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, ShellAPI, Vcl.ExtCtrls;

type
  TfAbout = class(TForm)
    memHelp: TMemo;
    pnlTop: TPanel;
    bntUpdate: TButton;
    lblInfo: TLabel;
    procedure bntUpdateClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fAbout: TfAbout;

implementation

{$R *.dfm}

procedure TfAbout.bntUpdateClick(Sender: TObject);
begin
  ShellExecute(handle, 'open', 'https://bitbucket.org/Attil/aion-dps-meter', nil, nil, SW_SHOW);
end;

end.

unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DB, DBGrids, ComCtrls, IniFiles,
  ImgList, DBCtrls, JPEG, ExtCtrls, ShellAPI,
  DBGridEh, MemTableEh, MemTableDataEh, GridsEh, DBGridEhGrouping,
  ToolCtrlsEh, DBAxisGridsEh,
  System.ImageList, System.UITypes, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  Vcl.Clipbrd, Vcl.Menus;

type
  TfrmMain = class(TForm)
    mtList: TMemTableEh;
    DataSource: TDataSource;
    mtListCharName: TStringField;
    mtListCharDamage: TIntegerField;
    mtListCharDots: TIntegerField;
    Status: TStatusBar;
    mtDots: TMemTableEh;
    mtDotsClassNo: TIntegerField;
    mtSkills: TMemTableEh;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    DBGrid: TDBGridEh;
    mtDotsSkillName: TWideStringField;
    mtDotsLastUsed: TStringField;
    mtListCharClass: TIntegerField;
    OpenDialog: TOpenDialog;
    ImgList: TImageList;
    mtListDmgAll: TIntegerField;
    ProgressBar: TProgressBar;
    pnlTop: TPanel;
    edtLogFileName: TEdit;
    btnCalc: TButton;
    btnCopyFormatted: TButton;
    btnClear: TButton;
    btnSettings: TButton;
    btnOpen: TButton;
    btnHelp: TButton;
    mtAdv: TMemTableEh;
    mtAdvChar: TStringField;
    mtAdvSkill: TStringField;
    mtAdvAttCnt: TIntegerField;
    mtAdvAttDmg: TIntegerField;
    mtAdvCritCnt: TIntegerField;
    mtListCritical: TCurrencyField;
    mtAdvClass: TWordField;
    btnCopyAllInOneLine: TButton;
    btnSkilllsStat: TButton;
    mtListAttCnt: TSmallintField;
    mtListCritCnt: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btnCalcClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCopyFormattedClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure DBGridTitleBtnClick(Sender: TObject; ACol: Integer; Column: TColumnEh);
    procedure btnSettingsClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnCopyAllInOneLineClick(Sender: TObject);
    procedure btnSkilllsStatClick(Sender: TObject);
    procedure RegHC;
    procedure UnregHC;
    procedure edtLogFileNameDblClick(Sender: TObject);

  private
    { Private declarations }
    procedure OnHotKey(var Msg: TWMHotKey); message WM_HOTKEY;
  public
    { Public declarations }
  end;

const
  NumbersSet = ['0'..'9'];
  Version = '1.3.1';

var
  frmMain: TfrmMain;

  cHotKey :integer=0;
  cHotKeyModifiers :integer=0;
  cHotKeyGI :Word;
  ProgramDir: string;
  optShowDots, optHideMobs, optCalcType: integer;
  AionCharClasses: array[0..12] of String;
  DotsDmgByClass: array[0..11] of integer;
  //YourSkillsCnt: array[0..12] of integer; //?
  sYoursCharName :string = '!��!';

implementation

uses uStat, uSettings, uAbout, uAdvanced;

{$R *.dfm}

// ������� ������ ����������
procedure RamClean;
var
  MainHandle: THandle;
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID);
    SetProcessWorkingSetSize(MainHandle, DWORD(-1), DWORD(-1));
    CloseHandle(MainHandle);
  end;
end;


//*****//
// ������� ��� ��������� ���������� �����
//*****//

{
// ���������� ������������ �� 2 ��������
function Max(a: integer; b: integer): integer;
begin
  if a > b then
    Result := a
  else
    Result := b;
end;



// ������� ������� ������� � ������
function CntChRepet(const sInputStr  :string; chSymbol :Char): integer;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to Length(sInputStr) do
    if sInputStr[i] = chSymbol then
      Inc(Result);
end;
}

// �������� �� ������ str ������ ������� � ����� sub1+1 � �� sub2-1
function CutFromStr(str :string; const sub1, sub2: string): string;
var
  StartPos, EndPos: Integer;
begin
  Result := '';

  {//?! ��� ������ �� ������: ������ ����-����� � ������ ������ � ��.
  StartPos := Pos(sub1, str);
  EndPos := Pos(sub2, str);
  if (StartPos > 0) and (EndPos > 0) then
  begin
    StartPos := StartPos+Length(sub1);
    Result := Copy(str, StartPos, EndPos - StartPos);
  end;  }

  repeat //?! ��������� �����������
    StartPos := Pos(sub1, str);
    if StartPos > 0 then
    begin
      str := Copy(str, StartPos + length(sub1), length(str) - 1);
      StartPos := 1;
      EndPos := Pos(sub2, str);
      if EndPos>0 then
        Result := Copy(str, StartPos, EndPos - StartPos)
      else
        Exit;
      str := Copy(str, EndPos + length(sub2), length(str) - 1);
    end;
  until StartPos <= 0;  //?!
end;

// ��������� �� �������������� ������ ���-����� ����� �����
function DmgCut(const buf: string): integer;
var
  i: Integer;
  str, str2: string;
  c: boolean;
begin

  str := CutFromStr(buf, ' : ', ' ��. ');
  if str='' then
  begin
    Result := 0;
    Exit;
  end;

  //?! �����-�� ������������ �����. ��� �� �������� Trim() � TryStrToInt() ?! �����������. �, �������� ��� ������� � ������ ���� ����� ������� ����� :D
  str2 := '';
  c := false;
  i := length(str);
  while c = false do
  begin
    if CharInSet(str[i], NumbersSet) then
    begin
      str2 := str[i] + str2;
      i := i - 1;
    end
    else
    if str[i] <> ' ' then
      i := i - 1
    else
      c := true;
  end;

  try //!!!
     Result := StrToInt(str2);
  except
     Result := 0;
  end;

end;

// ��������� �� �������������� ������ ��� ���������
function CharNameCut(const buf: string): string;
begin
  if pos('����������', buf) > 0 then
  begin
    if pos('����������� ����! ', buf) > 0 then
      Result := CutFromStr(buf, '����������� ����! ', ' ����������')
    else
      Result := CutFromStr(buf, ' : ', ' ����������')
  end
  else
  if (pos('����������', buf) > 0) and (pos('���������', buf) > 0) then
    Result := CutFromStr(buf, '��������� ', '.')
  else
  if pos('�������� �����', buf) > 0 then
    Result := CutFromStr(buf, ' : ', ' ��������')

  else
  if pos('�������', buf) > 0 then
  begin
    if pos('����������� ����! ', buf) > 0 then
      Result := CutFromStr(buf, '����������� ����! ', ' �������')
    else
      Result := CutFromStr(buf, ' : ', ' �������');
  end;
  //
  Result := Trim(Result);
end;

{// ���������� ���� ��� ��� (1-��, 0-���)
function WasCrit(const buf:string): integer;
begin
  Result := 0;
  if (pos('����������� ����!', buf)>0) then
    Result := 1;
end;
}

// ���������� �������� ������ �� �������������� ������
function SkillName(const buf: string): string;
var
  str: string;
begin
  if pos('����������', buf) > 0 then
  begin
    if pos(':', CutFromStr(buf, '����������', '.')) > 0 then
      str := CutFromStr(buf, '����������: ', '.')
    else
      str := CutFromStr(buf, '���������� ', '.')
  end
  else
  if pos('�������', buf) > 0 then //���� � ����������. ��� "����������" = "����� ������"
  begin //'���� �����' ������� 468 ��. ����� ���� ��������.
    if pos('����������� ����! ', buf) > 0 then
      str := CutFromStr(buf, '����������� ����! ', ' �������')
    else
      str := CutFromStr(buf, ': ', '�������')
  end
  else
  begin
    if (pos('����������� ����!', buf) > 0) then
      str := CutFromStr(buf, '����������� ����! ', ':')
    else
    begin
      if (pos('������: ', buf) > 0) then
        str := CutFromStr(buf, '������: ', ':')
      else
        str := CutFromStr(buf, ' : ', ':');
    end;
  end;
  Result := Trim(str);
end;

// ���������� �����. ��������� - �����    (0 �� �����)
function DefineClass(const buf: string): Integer;
var
  c_class :integer;
  sSkillName :string;
begin
  c_class := 0;
  sSkillName := SkillName(buf);
  // ���� � �������
  frmMain.mtSkills.First;
  repeat
    if Pos(frmMain.mtSkills.FieldByName('SkillName').AsString, sSkillName)>0 then // Pos>0 Added by AJ 22.06.2017: ���� ��������� ������� � ���� = ����� ������� ���������� �����
    begin
      c_class := frmMain.mtSkills.FieldByName('ClassNo').AsInteger;
      Break;
    end;
    frmMain.mtSkills.Next;
  until frmMain.mtSkills.Eof;

  // ���� � �����
  if c_class = 0 then
  begin
    frmMain.mtDots.First;
    repeat
      if Pos(frmMain.mtDots.FieldByName('SkillName').AsString, sSkillName)>0 then // Pos>0 Added by AJ 22.06.2017
      begin
        c_class := frmMain.mtDots.FieldByName('ClassNo').AsInteger;
        Break;
      end;
      frmMain.mtDots.Next;
    until frmMain.mtDots.Eof;
  end;

  // ��������, �� ��� �� ������������ �������
  // � ����� ����� ����� ���� � ���������, � � ������� - ���
  {//?! � � ����� ������ � �������� �������  = ����. � ������� ���� ������ � �������� ������� � ������ 4.x+
  for i := 1 to Length(sSkillName) do
    if (sSkillName[i] = 'I') or (sSkillName[i] = 'V') or (sSkillName[i] = 'X') then
      Mob := True; }
  {
  if (c_class<>0) and (Pos(' ',CharNameCut(buf)) > 0) then
     c_class := 0;
  }
  // ���������, �� �� ��
  if pos('���������� ����', buf) > 0 then
    c_class := 12;
  {// ��������� �����������  = ���� � ���� �����
  if (pos('����������', buf) > 0) and (pos('�������', buf) > 0) and (pos('����������', buf) < pos('�������', buf)) then
    c_class := 12;
  }
  // ��������� �������
  if (pos('������ ������� ����������', buf) > 0) and (pos('�������', buf) > 0) and (pos('������', buf) < pos('�������', buf)) then
    c_class := 12;
  // ��������� ����
  if (pos('������ ���� ����������', buf) > 0) and (pos('��������', buf) > 0) and (pos('������', buf) < pos('��������', buf)) then
    c_class := 12;
  Result := c_class;
end;

// ���������� ��� ��� ���
function SkillIsDot(const sSkillName: string; mtDots: TMemTableEh): boolean;
begin
  Result := False;

  //Result := mtDots.Locate('SkillName', sSkillName, [loPartialKey]); //?!

  // ���� � �����
  mtDots.First;
  repeat
    if Pos(mtDots.FieldByName('SkillName').AsString, sSkillName)>0  then // Pos>0 Added by AJ 22.06.2017
    begin
      Result := True;
      Break;
    end;
    mtDots.Next;
  until mtDots.Eof;
end;


// ���������� ����� ���������� ��� ����� �����-���
procedure SetLastUsedDots(mtDots: TMemTableEh; const cname, skill: string);
begin
  mtDots.First;
  repeat
    if Pos(mtDots.FieldByName('SkillName').AsString, skill)>0 then // Pos>0 Added by AJ 22.06.2017
    begin
      mtDots.Edit;
      mtDots.FieldByName('LastUsed').AsString := cname;
      mtDots.Post;
      Break;
    end;
    mtDots.Next;
  until ((mtDots.Eof));
end;

// ��������� ����� ���������� ��� ����� �����-��� (�� �������)
function GetLastUsedDots(mtDots: TMemTableEh; const skill: string): string;
begin
  Result := '';
  mtDots.First;
  repeat
    if Pos(mtDots.FieldByName('SkillName').AsString, skill)>0 then // Pos>0 Added by AJ 22.06.2017
    begin
      Result := mtDots.FieldByName('LastUsed').AsString;
      Break;
    end;
    mtDots.Next;
  until mtDots.Eof;
end;

// ���������� ���������� �� ������ � �������
procedure AddStatisticToTables(const LogLine, CharName, SkillName :string; CharClass, Damage, DotDamage :Integer);
var
  IsCrit: Boolean;
  iAttCnt, iCritCnt :Integer;
begin
  //���������� �������� ������� �������, ����� �� ������� - ����� ������ ������������.
  //����� ��������� ��� �� �������������� / ������ ����� ������ ������ �� ����� ��������� � �������� ������
  // = ���������� ������� � ����� � ������ ��������: Locate.
  //�� �������: RecordsView - ������� ������ � ��������������� ������.



  IsCrit := pos('����������� ����!', LogLine) > 0;

  if frmMain.mtList.Locate('CharName', CharName, []) then
  begin
    frmMain.mtList.Edit;

    iAttCnt := frmMain.mtList.FieldByName('AttCnt').AsInteger+1;
    frmMain.mtList.FieldByName('AttCnt').AsInteger := iAttCnt;

    iCritCnt := frmMain.mtList.FieldByName('CritCnt').AsInteger;
    if IsCrit then
    begin
      iCritCnt := iCritCnt+1;
      frmMain.mtList.FieldByName('CritCnt').AsInteger := iCritCnt;
    end;
    if iAttCnt>5 then //���������� ����, ��� ������ ������ 6�� ������� (�.�. �������� ���������� % ����� �� ����� ������ ���)
       frmMain.mtList.FieldByName('Critical').AsCurrency := (iCritCnt * 100) / iAttCnt;

    frmMain.mtList.FieldByName('CharDamage').AsInteger := frmMain.mtList.FieldByName('CharDamage').AsInteger + Damage;
    frmMain.mtList.FieldByName('CharDots').AsInteger := frmMain.mtList.FieldByName('CharDots').AsInteger + DotDamage;

    frmMain.mtList.FieldByName('DmgAll').AsInteger := frmMain.mtList.FieldByName('CharDamage').AsInteger + frmMain.mtList.FieldByName('CharDots').AsInteger;

    if CharClass > 0 then //���� �������� �����
    begin
      frmMain.mtList.FieldByName('CharClass').AsInteger := CharClass;
    end
    else
    begin
      if frmMain.mtList.FieldByName('CharClass').AsInteger=0 then
      begin
        if Pos(' ', CharName)=0 then //���� �� ���
           CharClass := DefineClass(LogLine);
        frmMain.mtList.FieldByName('CharClass').AsInteger := CharClass;
      end;
    end;

    frmMain.mtList.Post;
  end
  else
  begin
    frmMain.mtList.Insert;
    frmMain.mtList.FieldByName('CharName').AsString := CharName;
    frmMain.mtList.FieldByName('CharClass').AsInteger := CharClass;

    if IsCrit then
      frmMain.mtList.FieldByName('CritCnt').AsInteger := 1
    else
      frmMain.mtList.FieldByName('CritCnt').AsInteger := 0;

    frmMain.mtList.FieldByName('Critical').AsCurrency := 0.0;
    frmMain.mtList.FieldByName('AttCnt').AsInteger := 1;
    frmMain.mtList.FieldByName('CharDamage').AsInteger := Damage;
    frmMain.mtList.FieldByName('CharDots').AsInteger := DotDamage;
    frmMain.mtList.FieldByName('DmgAll').AsInteger := Damage + DotDamage;
    frmMain.mtList.Post;
  end;


  if frmMain.mtAdv.Locate('Char;Skill', VarArrayOf([CharName, SkillName]), []) then
  begin
    frmMain.mtAdv.Edit;

    frmMain.mtAdv.FieldByName('Class').AsInteger   := CharClass;
    frmMain.mtAdv.FieldByName('AttCnt').AsInteger  := frmMain.mtAdv.FieldByName('AttCnt').AsInteger + 1;
    frmMain.mtAdv.FieldByName('AttDmg').AsInteger  := frmMain.mtAdv.FieldByName('AttDmg').AsInteger + Damage + DotDamage; // v+x+0 = v+x

    if IsCrit then
      frmMain.mtAdv.FieldByName('CritCnt').AsInteger := frmMain.mtAdv.FieldByName('CritCnt').AsInteger + 1;

    frmMain.mtAdv.Post;
  end
  else
  begin
    frmMain.mtAdv.Insert;

    frmMain.mtAdv.FieldByName('Char').AsString := CharName;
    frmMain.mtAdv.FieldByName('Class').AsInteger   := CharClass;
    frmMain.mtAdv.FieldByName('Skill').AsString := SkillName;
    frmMain.mtAdv.FieldByName('AttCnt').AsInteger := 1;

    if IsCrit then
       frmMain.mtAdv.FieldByName('CritCnt').AsInteger := 1
    else
       frmMain.mtAdv.FieldByName('CritCnt').AsInteger := 0;

    frmMain.mtAdv.FieldByName('AttDmg').AsInteger := Damage + DotDamage;

    frmMain.mtAdv.Post;
  end;

end;



{
// ���������� ���������� �� ������ � ������� �����
procedure UpdateList(mt: TMemTableEh; cclass: integer; const cname, buf: string; dmg, dmgdot: integer);
var
  InList: Boolean;
  //i, yc, ycc: integer;
begin
  InList := False;

  //mt.First;
  //repeat
  //  if mt.FieldByName('CharName').AsString = cname then
  if mt.Locate('CharName', cname, []) then
  begin
    InList := True;
    mt.Edit;
    mt.FieldByName('CharDamage').AsInteger := mt.FieldByName('CharDamage').AsInteger + dmg;
    mt.FieldByName('CharDots').AsInteger := mt.FieldByName('CharDots').AsInteger + dmgdot;
    mt.FieldByName('DmgAll').AsInteger := mt.FieldByName('CharDamage').AsInteger + mt.FieldByName('CharDots').AsInteger;

    if cclass > 0 then //���� �������� �����
      mt.FieldByName('CharClass').AsInteger := cclass
    else
    begin
      if mt.FieldByName('CharClass').AsInteger=0 then
        mt.FieldByName('CharClass').AsInteger := DefineClass(buf);
    end;

    {if cname = sYoursCharName then  //?! �� ��� �����?
    begin
      yc := cclass;
      ycc := 0;
      for i := 1 to 12 do
        if YourSkillsCnt[i] > ycc then
        begin
          yc := i;
          ycc := YourSkillsCnt[i];
        end;
      mt.FieldByName('CharClass').AsInteger := yc;
    end; }

    {
    mt.Post;
    //Break;//~
  end;
  //  mt.Next;
  //until mt.Eof;
  //
  if not InList then
  begin
    mt.Insert;
    mt.FieldByName('CharName').AsString := cname;
    mt.FieldByName('CharDamage').AsInteger := dmg;
    mt.FieldByName('CharDots').AsInteger := dmgdot;
    mt.FieldByName('DmgAll').AsInteger := dmg + dmgdot;
    mt.FieldByName('CharClass').AsInteger := cclass;
    mt.Post;
  end;
end;

// ���������� ������� � ��� �����������
procedure UpdateAdv(mtAdv: TMemTableEh; const NChar, Skill: string; dmg, crit: integer);
var
  InList: Boolean;
begin
  InList := False;

  //���������� �������� ������� �������, ����� �� ������� - ����� ������ ������������.
  //����� ��������� ��� �� �������������� / ������ ����� ������ ������ �� ����� ��������� � �������� ������
  // = ���������� ������� � ����� � ������ ��������: Locate

  //RecordsView - ������� ������ � ��������������� ������

  //mtAdv.First;
  //repeat
  //  if (mtAdv.FieldByName('Char').AsString = NChar) and (mtAdv.FieldByName('Skill').AsString = Skill) then

  if mtAdv.Locate('Char;Skill', VarArrayOf([NChar,Skill]), []) then
  begin
    InList := True;
    mtAdv.Edit;
    mtAdv.FieldByName('AttCnt').AsInteger := mtAdv.FieldByName('AttCnt').AsInteger + 1;
    mtAdv.FieldByName('AttDmg').AsInteger := mtAdv.FieldByName('AttDmg').AsInteger + dmg;
    mtAdv.FieldByName('CritCnt').AsInteger := mtAdv.FieldByName('CritCnt').AsInteger + crit;
    mtAdv.Post;
    //Break;
  end;
  //  mtAdv.Next;
  //until mtAdv.Eof;


  if not InList then
  begin
    mtAdv.Insert;
    mtAdv.FieldByName('Char').AsString := NChar;
    mtAdv.FieldByName('Skill').AsString := Skill;
    mtAdv.FieldByName('AttCnt').AsInteger := 1;
    mtAdv.FieldByName('CritCnt').AsInteger := crit;
    mtAdv.FieldByName('AttDmg').AsInteger := dmg;
    mtAdv.Post;
  end;

end;
}

//*****//

// ������ ������� ������
procedure ParseLogLine(const sLogLine: string; mtList, mtDots, mtSkills, mtAdv: TMemTableEh; DotsCalcType:Integer);
var
  sCharNameLastUsed, Element: string;
  sCharName, sSkillName :string;
  iDmg{, iClassID} :Integer;
begin
  // ���������, ��������� �� � ������ ����
  if (pos('��. ', sLogLine) > 0) and (pos('�����', sLogLine) > 0) then
  begin

    //+++Debug!
    //if (pos('�������� �������', sLogLine) > 0) then
    //   ShowMessage('�������� �������');
    //---Debug

    if (Pos(' [Protector] ', sLogLine)>0) then //xxxx.xx.xx xx:xx:xx : Darislava: [Protector] �������� ������ ���� ������ -2147483648 ��. �����.
       Exit;  //"��������" ��������� ���� / ������ �������

    //���������� �� ������ ���-�� �����
    iDmg := dmgcut(sLogLine);//0 - ������ ����� ������ �������������� / "��������" ��������� ����
    if (iDmg>600000) or (iDmg=0) or (iDmg<0) then  //'xxxx.xx.xx xx:xx:xx : Pashundra �������� ����� � ������� ��������� Levisnyper 2�127�637�309 ��. �����. '
        Exit;  // - ��� ���� �� ��������� ��������: �������� ����� �� ����������.
    if ( (pos('�������� ����� � ������� ���������', sLogLine) > 0) and (iDmg>10000) ) then //"��������" ��������� ����
        Exit;  //Lightekiller �������� ����� � ������� ��������� ������� 279�812 ��. �����.
    // ����������: ���������
    if pos('�� �������', sLogLine) > 0 then
    begin
      //if iDmg>3000 then
      //   ShowMessage(IntToStr(iDmg)+'  |  '+sLogLine);
      AddStatisticToTables(sLogLine, sYoursCharName, '���������', 0, iDmg, 0);
    end
    else
    if (pos('�������', sLogLine) > 0) then //��������� ������ + ���� + ����������
    begin
      sSkillName  := CharNameCut(sLogLine); //'���� �����' ������� 468 ��. ����� ���� ��������.
      if SkillIsDot(sSkillName, mtDots) then
      begin //'���������� �����' ������� 291 ��. ����� ���� ���������-���������� ������ ���������.
        if DotsCalcType=0 then
        begin
          sCharNameLastUsed := GetLastUsedDots(mtDots, sSkillName);
          if sCharNameLastUsed <> '' then
          begin
            AddStatisticToTables(sLogLine, sCharNameLastUsed, sSkillName, 0, 0, iDmg);
          end
          else
          begin
            Inc(DotsDmgByClass[DefineClass(sLogLine)], iDmg);
          end;
        end
        else
          Inc(DotsDmgByClass[DefineClass(sLogLine)], iDmg);
      end
      else // ������� ���������
      begin
        AddStatisticToTables(sLogLine, CharNameCut(sLogLine), '���������', 0, iDmg, 0);
      end;
    end
    else
    // ����
    if pos('���������� ����', sLogLine) > 0 then
    begin
      AddStatisticToTables(sLogLine, CutFromStr(sLogLine, ' : ', ': '), '��', 12, 0, iDmg); //��� � ������� ����� ����������
    end
    else
    // ������ � ���� - ���� ��� ����� ��� ���������
    if (pos('����������', sLogLine) > 0) then
    begin
      sCharName  := CharNameCut(sLogLine);
      sSkillName := SkillName(sLogLine);
      if ( Pos(sCharName, sSkillName)=0 ) then //��� ��������� <> ��� ������  if (sCharName<>sSkillName) then
      begin
        if SkillIsDot(sSkillName, mtDots) then
        begin
          //UpdateAdv(mtAdv, sCharName, sSkillName, iDmg, WasCrit(buf));

          if DotsCalcType=0 then
          begin
            SetLastUsedDots(mtDots, sCharName, sSkillName);
            AddStatisticToTables(sLogLine, sCharName, sSkillName, 0, 0, iDmg);
          end
          else //���� ����� �2 = � �������������� ���������� ����� �� ��������
            Inc(DotsDmgByClass[DefineClass(sLogLine)], iDmg);
        end
        else//Skill NOT Dot
        begin
          AddStatisticToTables(sLogLine, sCharName, sSkillName, 0, iDmg, 0);
        end;
      end
      else
      begin // ���� ��� ��������� = ��� ������ (��������, ������� ����)
        if DotsCalcType=0 then
        begin
          sCharNameLastUsed := GetLastUsedDots(mtDots, sSkillName);
          if sCharNameLastUsed <> '' then
          begin
            AddStatisticToTables(sLogLine, sCharNameLastUsed, sSkillName, 0, 0, iDmg);
          end
          else //!!!
          begin                            //!!!
            //AddStatisticToTables(sLogLine, sCharName, sSkillName, 0, 0, iDmg);
            Inc(DotsDmgByClass[DefineClass(sLogLine)], iDmg); //!!!
            //TODO: xxxx.xx.xx xx:xx:xx : Frakch ����������: ������: ������ ���� V. ������ ���� ����������� ��� ����� ���� ����������������� �����.
            //xxxx.xx.xx xx:xx:xx : ������ ���� ����������: ������: ������ ���� V. ����������������� ����� �������� 694 ��. �����.
            //sCharName = ������ ����
            //sSkillName = ������: ������ ���� V
          end;
        end
        else //DotsCalcType=1
          Inc(DotsDmgByClass[DefineClass(sLogLine)], iDmg);
      end;
    end
    else
    begin
      //���� ��� ����� ��������� (���� � �����)
      sSkillName := SkillName(sLogLine);
      if SkillIsDot(sSkillName, mtDots) then
      begin
        if DotsCalcType=0 then
        begin
          sCharNameLastUsed := GetLastUsedDots(mtDots, sSkillName);
          if sCharNameLastUsed <> '' then
          begin
            AddStatisticToTables(sLogLine, sCharNameLastUsed, sSkillName, 0, 0, iDmg);
          end
          else
          begin //!!!
            Inc(DotsDmgByClass[DefineClass(sLogLine)], iDmg); //!!!

            //SetLastUsedDots(mtDots, sYoursCharName, sSkillName);
            //AddStatisticToTables(sLogLine, sYoursCharName, sSkillName, 0, 0, iDmg); //?!
          end;
        end
        else
          Inc(DotsDmgByClass[DefineClass(sLogLine)], iDmg);
      end
      else
      begin // ����������: ������ (�� ����)
        if (pos('��������', sLogLine) > 0) and (pos('�������� ������ ����', sLogLine) = 0) then
        begin                             //��������� ��� ����: '����.��.�� ��:��:�� : Flashonthenigth: [Protector] �������� ������ ���� Inotik 1�485 ��. �����. '
          if ( (pos('�������: ����������', sLogLine) > 0) or ((pos('�������: ������ ������', sLogLine) > 0) )) then
          begin // ���� ��� ����������, �� ���� ����������� �����������
            Element := CutFromStr(sLogLine, '(', ')');

            if Element = '�����' then
              AddStatisticToTables(sLogLine, '���������� ����', SkillName(sLogLine), 12, iDmg, 0)
            else
            if Element = '�����' then
              AddStatisticToTables(sLogLine, '���������� �����', SkillName(sLogLine), 12, iDmg, 0)
            else
            if Element = '����' then
              AddStatisticToTables(sLogLine, '���������� ����', SkillName(sLogLine), 12, iDmg, 0)
            else
            if Element = '������' then
              AddStatisticToTables(sLogLine, '���������� �������', SkillName(sLogLine), 12, iDmg, 0)
            else
            begin
              //ShowMessage(IntToStr(iDmg)+'  |  '+sLogLine);
              AddStatisticToTables(sLogLine, '���������� ???', SkillName(sLogLine), 12, iDmg, 0);
            end;

          end
          else //�� ����������
          begin
            //if pos('�������', sLogLine) > 0 then
            //   ShowMessage(IntToStr(iDmg)+'  |  '+sLogLine);
            AddStatisticToTables(sLogLine, sYoursCharName, SkillName(sLogLine), 0, iDmg, 0);
          end;
        end;
      end;
    end;
  end
  else
  begin //if (pos('��. ', buf) = 0) and (pos('�����', buf) = 0)
    if DotsCalcType=0 then
    begin
      // ���������, �������������� �� ������-���� ��� ����� � ����� <> ��� ���������
      if (pos('����������', sLogLine) > 0) then
      begin
        sCharName  := CharNameCut(sLogLine);
        sSkillName := SkillName(sLogLine);
        if ( Pos(sCharName, sSkillName)=0 ) and SkillIsDot(sSkillName, mtDots) then
          SetLastUsedDots(mtDots, sCharName, sSkillName);
      end;
      // ���������, �������������� �� ������� ��� �����
      //Add = ������� ������� I: ���������� ���������� ������� ��������� Bloamer. = AL Bug //!!!
      if (pos('����������', sLogLine) > 0) and (pos('���������', sLogLine) > 0) then
      begin
        sSkillName := SkillName(sLogLine);
        if SkillIsDot(sSkillName, mtDots) then
          SetLastUsedDots(mtDots, CharNameCut(sLogLine), sSkillName);
      end;
    end;
  end;
end;

{
�� ���������:
Miurry ����������: ������: ��������� �������� IV. ��������� �������� ����������� ��� ����� ���� ��������.
��������� �������� ����������: ������: ������ ���� IV. �������� �������� 454 ��. �����.
+
�������� ����������: ������: ���� ����� IV. ���� ����� ����������� ��� ����� ���� ��������.
���� ����� ������� 468 ��. ����� ���� ��������.

+
������: ���������� ����� II: ���������� ���������� ����� ��������� ��������.
���������� ����� ������� 291 ��. ����� ���� ���������-���������� ������ ���������.
}

// ����� ���� � ������� ����� ��������
procedure DivideDotsToAll(mtList: TMemTableEh);
var
  i, iDmg: integer;
  AionCharClasses :Integer;
  InListClassCount: array[0..11] of word;
  isZeroDotsDmg :Boolean;
begin
  isZeroDotsDmg := True;

  for i := 0 to 11 do
  begin
    if DotsDmgByClass[i]>0 then
    begin
      isZeroDotsDmg := False;
      Break;
    end;
  end;

  if isZeroDotsDmg then
    Exit;

  for i := 0 to 11 do
    InListClassCount[i] := 0;

  // ������� ������� ����� ������� � �������
  mtList.First;
  repeat
    AionCharClasses := mtList.FieldByName('CharClass').AsInteger;
    if (AionCharClasses>= 0) and (AionCharClasses <= 11) then
      Inc(InListClassCount[AionCharClasses]);
    mtList.Next;
  until mtList.Eof;

  // ����� ���� ����� ����������� ������ ������
  // � ������� ���� � ����� ����� ���������
  mtList.First;
  repeat
    AionCharClasses := mtList.FieldByName('CharClass').AsInteger;
    if (AionCharClasses >= 0) and (AionCharClasses <= 11) and (DotsDmgByClass[AionCharClasses]>0) then
    begin
      iDmg := (DotsDmgByClass[AionCharClasses] div InListClassCount[AionCharClasses]);
      AddStatisticToTables('', mtList.FieldByName('CharName').AsString, '����� "�����" �����', 0, 0, iDmg);
    end;
    mtList.Next;
  until mtList.Eof;
end;

{
// ������� % ����� �������
procedure CalcCritPercent(mtList, mtAdv: TMemTableEh);
var
  CAtt, CCrit: integer;
  CharName: string;
begin
  mtList.First;
  while not (mtList.Eof) do
  begin
    CAtt := 0;
    CCrit := 0;
    CharName := mtList.FieldByName('CharName').AsString;

    mtAdv.First;
    mtAdv.Locate('Char', CharName, []);
    while not (mtAdv.Eof) do
    begin
      if mtAdv.FieldByName('Char').AsString = CharName then
      begin
        Inc(CAtt, mtAdv.FieldByName('AttCnt').AsInteger);
        Inc(CCrit, mtAdv.FieldByName('CritCnt').AsInteger);
      end
      else
        Break; //� ��� ��� � ����� ������� ������� ������������� �� ����� ������ � �������� �������
      mtAdv.Next;
    end;

    // ����� %
    if CAtt <> 0 then
    begin
      mtList.Edit;
      //mtList.FieldByName('AttCnt').AsInteger := CAtt;
      mtList.FieldByName('Critical').AsCurrency := (CCrit * 100) / CAtt;
      mtList.Post;
    end;

    mtList.Next;
  end;
end;
}

// ���������� ����� � �������� �������
procedure AdvGetClass(mtList, mtAdv: TMemTableEh);
var
  sCharName, sPrevCharName  :string;
  iCharClass, iPrevCharClass :Integer;
begin
  sPrevCharName  := '';
  iPrevCharClass := 0;

  mtAdv.First;
  while not (mtAdv.Eof) do
  begin
    iCharClass := mtAdv.FieldByName('Class').AsInteger;

    if iCharClass = 0 then
    begin
      sCharName  := mtAdv.FieldByName('Char').AsString;

      if sCharName = sPrevCharName then
      begin
         iCharClass := iPrevCharClass;
      end
      else
      // ���� � �������� ������� �� ����� ���������
      if mtList.Locate('CharName', sCharName, []) then
      begin
        sPrevCharName := sCharName;
        iCharClass :=  mtList.FieldByName('CharClass').AsInteger;
        iPrevCharClass := iCharClass;
      end;

      //if iCharClass <> 0 then
      //begin
        mtAdv.Edit;
        mtAdv.FieldByName('Class').AsInteger := iCharClass;
        mtAdv.Post;
      //end;

    end;

    mtAdv.Next;
  end;
end;

////////////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////
// ��������� ������� ������ "����������"

procedure TfrmMain.btnCalcClick(Sender: TObject);
var
  f: textfile;
  buf: string;
  i, max_i :integer;
  StartTime: TDateTime;
  StateOfListFilter: boolean;
  FileLinesCount :Integer;
  //AionCharClasses :Integer;
  //DmgAll :Int64;
begin
  StartTime := Now;
  mtList.EmptyTable;
  mtAdv.EmptyTable;
  // ���������� ��������� ������� � ��������� ���
  StateOfListFilter := mtList.Filtered;
  mtList.Filtered := False;

  // �������� ������� (���������� �������������� ���� ������� �� �������) � ����
  //for i := 0 to 12 do
  //  YourSkillsCnt[i] := 0;

  for i := 1 to 11 do
    DotsDmgByClass[i] := 0;

  // �������� ���������, ��� ����������� ����
  mtDots.First;
  repeat
    mtDots.Edit;
    mtDots.FieldByName('LastUsed').Clear;
    mtDots.Post;
    mtDots.Next;
  until mtDots.Eof;

  // ��������� � ������������ ����
  if FileExists(edtLogFileName.Text) then
  begin
    btnCalc.Enabled := False;
    btnClear.Enabled := False;

    AssignFile(f, edtLogFileName.Text);
    Reset(f);

    progressbar.Position := 0;
                                        //1.34
    FileLinesCount := round(FileSize(f) * 1.33); //~ 170,24b = 1.33*RecordSize(128b) by one line in aion chat log. (FileSize(f) * 1.33) =~LinesCount in log file
    progressbar.Max := 1000;
    max_i := FileLinesCount div 1000; //����� ����� ���-�� ����� �������� ����� �������� �� 1, ���� � ��� ������������ �������� � progressbar ����� 1000 (��� ����������� ����������� ����������)

    Status.Panels[0].Text := '����������� ���...';

    frmAdvanced.DS.Enabled := False;
    DataSource.Enabled := False;
    frmStat.DS.Enabled := False;

    i := 0;
    // �� ������ ������ log �����
    while not EOF(f) do
    begin
      inc(i);
      if i = max_i then // ����� ��������� ������ max_i ����� ���������� ��������
      begin
        Application.ProcessMessages;
        progressbar.StepBy(1);
        i := 0;
      end;
      readln(f, buf);
      // ����������� ������ � ����������� �� ������ ������� �� ��������
      ParseLogLine(buf, mtList, mtDots, mtSkills, mtAdv, optCalcType);
    end;

    CloseFile(f);

    //if optCalcType = 1 then  //������ �����, �.�. � "��������" ���� ������������ �����, ���� �� ������� ���������� ��� ���������, ������� ������� ���
       DivideDotsToAll(mtList);
    //
    AdvGetClass(mtList, mtAdv);
    mtAdv.SortByFields('Char Asc,Skill Asc');

    // �������� � ����������� ���. ����������
    //CalcCritPercent(mtList, mtAdv);

    frmAdvanced.DS.Enabled := True;
    mtAdv.First;

    // ��������� �� �����
    mtList.SortByFields('DmgAll Desc');
    DBGrid.Columns[2].Title.SortMarker := smDownEh;

    //"�����" ����
    {DmgAll := 0;
    i := 0;
    mtList.First;
    repeat
      AionCharClasses := mtList.FieldByName('CharClass').AsInteger;
      if (AionCharClasses >= 1) and (AionCharClasses <= 11) then
      begin
        DmgAll := DmgAll+mtList.FieldByName('DmgAll').AsInteger;
      end;
      mtList.Next;
    until mtList.Eof or (i>40);
    ShowMessage('All: '+IntToStr(DmgAll));}

    // �������� � �������� ������ ��� ���
    mtList.Filtered := StateOfListFilter;

    frmStat.DS.Enabled := True;
    DataSource.Enabled := True;
    mtList.First;

    progressbar.Position := progressbar.Max;
    Status.Panels[0].Text := '������! ����� ����������: ' + TimeToStr(Now - StartTime);
    btnCalc.Enabled := True;
    btnClear.Enabled := True;
  end
  else
    Status.Panels[0].Text := '������ Log ����� �� ����������!';

  progressbar.Position := 0;

  RamClean;
end;



procedure TfrmMain.btnSkilllsStatClick(Sender: TObject);
begin
  if frmAdvanced.Visible then
     frmAdvanced.Hide
  else
     frmAdvanced.Show;
end;

// ��� �������� �����
procedure TfrmMain.FormCreate(Sender: TObject);
var
  i, cn: integer;
  f: textfile;
  IniFile: TIniFile;
  buf: string;
begin
  frmMain.Caption := 'DPS-meter ��� Aion (RU) Ver ' + Version;


  ProgramDir := GetCurrentDir;

  IniFile := TIniFile.Create(ProgramDir + '\options.ini');
  frmMain.Top := IniFile.ReadInteger('DESKTOP', 'Top', frmMain.Top);
  frmMain.left := IniFile.ReadInteger('DESKTOP', 'Left', frmMain.Left);
  frmMain.Width := IniFile.ReadInteger('DESKTOP', 'Width', frmMain.Width);
  frmMain.Height := IniFile.ReadInteger('DESKTOP', 'Height', frmMain.Height);
  edtLogFileName.Text := IniFile.ReadString('OPTIONS', 'Path', edtLogFileName.Text);
  optCalcType := IniFile.ReadInteger('OPTIONS', 'CalcType', optCalcType);
  optShowDots := IniFile.ReadInteger('VIEW', 'ShowDots', 0);
  optHideMobs := IniFile.ReadInteger('VIEW', 'HideMobs', 0);
  cHotKey := IniFile.ReadInteger('OPTIONS', 'HotKeyCode', 122);
  cHotKeyModifiers := IniFile.ReadInteger('OPTIONS', 'HotKeyCodeModifiers', 0);
  sYoursCharName := IniFile.ReadString('OPTIONS', 'YoursCharName', sYoursCharName);
  IniFile.Free;

  RegHC;

  // ������ ����������
  AionCharClasses[0] := '���/�������';
  AionCharClasses[1] := '���������';
  AionCharClasses[2] := '�����';
  AionCharClasses[3] := '������';
  AionCharClasses[4] := '�������';
  AionCharClasses[5] := '�����������';
  AionCharClasses[6] := '���������';
  AionCharClasses[7] := '�������';
  AionCharClasses[8] := '��������';
  AionCharClasses[9] := '����';
  AionCharClasses[10] := '�������';
  AionCharClasses[11] := '�����';
  AionCharClasses[12] := '��,���������';

  // �������� ������ � ����
  mtDots.EmptyTable;
  mtSkills.EmptyTable;

  // ��������� ����
  cn := 0;
  AssignFile(f, GetCurrentDir + '\dots.txt');
  Reset(f);
  while not EOF(f) do
  begin
    readln(f, buf);
    if pos('[', buf) > 0 then
    begin
      for i := 1 to 11 do
      begin
        if AionCharClasses[i] = CutFromStr(buf, '[', ']') then
        begin
          cn := i;
          Break;
        end;
      end;
    end
    else
    begin
      mtDots.Insert;
      mtDots.FieldByName('ClassNo').AsInteger := cn;
      mtDots.FieldByName('SkillName').AsString := buf;
      mtDots.FieldByName('LastUsed').AsString := '';
      mtDots.Post;
    end;
  end;
  CloseFile(f);

  // ��������� ������
  cn := 0;
  AssignFile(f, GetCurrentDir + '\skills.txt');
  Reset(f);
  while not EOF(f) do
  begin
    readln(f, buf);
    if pos('[', buf) > 0 then
    begin
      for i := 1 to 11 do
      begin
        if AionCharClasses[i] = CutFromStr(buf, '[', ']') then
        begin
          cn := i;
          Break;
        end;
      end;
    end
    else
    begin
      mtSkills.Insert;
      mtSkills.FieldByName('ClassNo').AsInteger := cn;
      mtSkills.FieldByName('SkillName').AsString := buf;
      mtSkills.Post;
    end;
  end;
  CloseFile(f);

  // ��������� ���� � ������
  mtDots.SortByFields('ClassNo');
  mtSkills.SortByFields('ClassNo');
end;


// ��� �������� ����� - ��������� ���������
procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  IniFile :TIniFile;
begin
  UnRegHC;
  // ���������� ��������
  IniFile := TIniFile.Create(ProgramDir + '\options.ini');
  IniFile.WriteInteger('DESKTOP', 'Top', frmMain.Top);
  IniFile.WriteInteger('DESKTOP', 'Left', frmMain.Left);
  IniFile.WriteInteger('DESKTOP', 'Width', frmMain.Width);
  IniFile.WriteInteger('DESKTOP', 'Height', frmMain.Height);
  IniFile.WriteString('OPTIONS', 'Path', edtLogFileName.Text);
  IniFile.WriteInteger('OPTIONS', 'CalcType', optCalcType);
  IniFile.WriteInteger('VIEW', 'ShowDots', optShowDots);
  IniFile.WriteInteger('VIEW', 'HideMobs', optHideMobs);
  IniFile.WriteInteger('OPTIONS', 'HotKeyCode', cHotKey);
  IniFile.WriteInteger('OPTIONS', 'HotKeyCodeModifiers', cHotKeyModifiers);
  IniFile.WriteString('OPTIONS', 'YoursCharName', sYoursCharName);
  IniFile.Free;
end;

// ������� �� ������ "��������"
procedure TfrmMain.btnClearClick(Sender: TObject);
begin
  mtList.EmptyTable;
  mtAdv.EmptyTable;
  Progressbar.Position := 0;
  if not FileExists(edtLogFileName.Text) then
  begin
     Status.Panels[0].Text := '������ Log ����� �� ����������!';
     Exit;
  end;

  if DeleteFile(edtLogFileName.Text) then
    Status.Panels[0].Text := 'Chat.log ������� ������!'
  else
    Status.Panels[0].Text := '���� �� �����! ��� ������:' + IntToStr(GetLastError);
end;

// ����������� ������ �� ������� � ����� ������ (� ���� ������ ��� ����)
procedure TfrmMain.btnCopyAllInOneLineClick(Sender: TObject);
var
  sTmp :string;
begin
  sTmp := '';

  mtList.First;
  repeat
    if (mtList.FieldByName('CharClass').AsInteger > 0)
      and (mtList.FieldByName('CharClass').AsInteger < 12) then
    begin
      sTmp := sTmp + mtList.FieldByName('CharName').AsString+'='+
                 mtList.FieldByName('DmgAll').AsString+' ';
    end;
    mtList.Next;
  until mtList.Eof;

  if Length(sTmp)<255 then //������ Aion �� ��������� ��������� ������ 256 �������� � ������ ����
  begin
    //���������� � ������ ����� �� �� / ����������� / ������� / �����
    mtList.First;
    repeat
      if mtList.FieldByName('CharClass').AsInteger=12 then
      begin
        sTmp := sTmp + mtList.FieldByName('CharName').AsString+'-'+
                   mtList.FieldByName('DmgAll').AsString+' ';
      end;
      mtList.Next;
    until mtList.Eof;
  end;

  //"��������" �� 256 ��������
  if Length(sTmp)>256 then
     Delete(sTmp, 256, Length(sTmp));

  Clipboard.AsText := sTmp;
end;

// ����������� ������ �� ������� � ����� ������ (� ���������������)
procedure TfrmMain.btnCopyFormattedClick(Sender: TObject);
var
  slTmp :TStringList;
begin
  slTmp := TStringList.Create;
  slTmp.Add('���� ����������(� �.�. ����):');

  mtList.First;
  repeat
    if (mtList.FieldByName('CharClass').AsInteger > 0) //?! �� ��������� � ������ ����� � ����������� ������?
      and (mtList.FieldByName('CharClass').AsInteger < 12) then
    begin
      slTmp.Add(mtList.FieldByName('CharName').AsString + ' - ' + mtList.FieldByName('DmgAll').AsString
                 + ' (' + mtList.FieldByName('CharDots').AsString + ')');
    end;
    mtList.Next;
  until mtList.Eof;

  slTmp.Add('');
  slTmp.Add('���� �� �� / ����������� / ������� / �����:');

  mtList.First;
  repeat
    if mtList.FieldByName('CharClass').AsInteger = 12 then
    begin
      slTmp.Add(mtList.FieldByName('CharName').AsString + ' - ' + mtList.FieldByName('DmgAll').AsString);
    end;
    mtList.Next;
  until mtList.Eof;

  Clipboard.AsText := slTmp.Text;
  slTmp.Free;
end;

procedure TfrmMain.btnHelpClick(Sender: TObject);
begin
  if fAbout.Visible then
    fAbout.Hide
  else
  begin
    fAbout.lblInfo.Caption := frmMain.Caption;
    fAbout.Left := frmMain.Left + 5;
    fAbout.Top := frmMain.Top + 30;
    fAbout.Show;
  end;
end;

// �������� log �����
procedure TfrmMain.btnOpenClick(Sender: TObject);
begin
  if OpenDialog.Execute then
    edtLogFileName.Text := OpenDialog.FileName;
end;

// ���������� �� ������ �� �������� �������
procedure TfrmMain.DBGridTitleBtnClick(Sender: TObject; ACol: Integer; Column: TColumnEh);
var
  sSortMarker :string;
begin
  if DBGrid.Columns[ACol].Title.SortMarker = smNoneEh then
     DBGrid.Columns[ACol].Title.SortMarker := smDownEh;

  if DBGrid.Columns[ACol].Title.SortMarker = smDownEh then
     sSortMarker := ' Asc'
  else
     sSortMarker := ' Desc';

  case ACol of
    0: mtList.SortByFields('CharClass'+sSortMarker);
    1: mtList.SortByFields('CharName'+sSortMarker);
    2: mtList.SortByFields('DmgAll'+sSortMarker);
    3: mtList.SortByFields('CharDamage'+sSortMarker);
    4: mtList.SortByFields('CharDots'+sSortMarker);
    5: mtList.SortByFields('AttCnt'+sSortMarker);
    6: mtList.SortByFields('Critical'+sSortMarker);
  end;
end;

procedure TfrmMain.edtLogFileNameDblClick(Sender: TObject);
begin
  ShellExecute(handle, 'open', PWideChar(ExtractFilePath(edtLogFileName.Text)), nil, nil, SW_SHOW);
end;

procedure TfrmMain.btnSettingsClick(Sender: TObject);
begin
  if fSettings.Visible then
    fSettings.Hide
  else
    fSettings.Show;
end;

procedure TfrmMain.RegHC;
begin
  // ���������. ���. �������
  if cHotKey=0 then
     Exit;

  // ��������� ����������� �������������� ��� ���������� ������
  cHotKeyGI := GlobalAddAtom('AionDPSMeterHotKey');

  if not RegisterHotKey(Handle, cHotKeyGI, cHotKeyModifiers, cHotKey) then
  begin
    fSettings.HotKey.HotKey := 0;
    ShowMessage('�� ������� ���������������� ����� ������� ������� ��� ������� ����!');
  end;
end;

procedure TfrmMain.UnregHC;
begin
  // ������ ����������� HotKey
  UnregisterHotKey(Handle, cHotKeyGI);
  GlobalDeleteAtom(cHotKeyGI);
end;

// ������� ������� ��� ������� ����
procedure TfrmMain.OnHotKey(var Msg: TWMHotKey);
begin
  inherited;
   // ������� ���
  btnClear.Click;
   // �������� �������������
  MessageBeep(MB_ICONINFORMATION); //  MB_ICONSTOP
end;

end.

program DPSMeter;

uses
  Forms,
  Vcl.Themes,
  Vcl.Styles,
  uMain in '.\Forms\uMain.pas' {frmMain},
  uStat in '.\Forms\uStat.pas' {frmStat},
  uSettings in '.\Forms\uSettings.pas' {fSettings},
  uAbout in '.\Forms\uAbout.pas' {fAbout},
  uAdvanced in '.\Forms\uAdvanced.pas' {frmAdvanced};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Aion DPS Meter';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmStat, frmStat);
  Application.CreateForm(TfSettings, fSettings);
  Application.CreateForm(TfAbout, fAbout);
  Application.CreateForm(TfrmAdvanced, frmAdvanced);
  Application.Run;
end.
